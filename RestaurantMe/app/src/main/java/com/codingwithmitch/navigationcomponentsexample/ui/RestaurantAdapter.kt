package com.codingwithmitch.navigationcomponentsexample.ui

import android.util.Log
import android.view.ViewGroup
import android.widget.Button
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelStore
import androidx.lifecycle.ViewModelStoreOwner
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.codingwithmitch.navigationcomponentsexample.R
import com.codingwithmitch.navigationcomponentsexample.model.LoginTableModel
import com.codingwithmitch.navigationcomponentsexample.model.LoginViewModel
import com.codingwithmitch.navigationcomponentsexample.model.Restaurant
import com.codingwithmitch.navigationcomponentsexample.model.Supplier
import kotlinx.android.synthetic.main.card_item.view.*
import java.lang.reflect.Array.get
import java.util.Observer


class RestaurantAdapter(var viewModel: LoginViewModel, val lifecycleOwner: LifecycleOwner, var currentList: MutableList<Restaurant>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        val holder = RestaurantListViewHolder.create(parent, viewModel, lifecycleOwner)
        return holder
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val restaurantItem = currentList[position]
        if (restaurantItem != null) {
            (holder as RestaurantListViewHolder).bind(restaurantItem)
        }
    }

    companion object {
        private val RESTAURANT_COMPARATOR = object : DiffUtil.ItemCallback<Restaurant>() {
            override fun areItemsTheSame(oldItem: Restaurant, newItem: Restaurant): Boolean =
                oldItem.idd == newItem.idd

            override fun areContentsTheSame(oldItem: Restaurant, newItem: Restaurant): Boolean =
                oldItem.idd == newItem.idd
        }
    }

    override fun getItemCount(): Int {
        return currentList.count()
    }

    fun submitList(getList: List<Restaurant>) {
        currentList = getList as MutableList<Restaurant>
    }
}