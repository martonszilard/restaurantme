package com.codingwithmitch.restaurantME.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Users")
data class LoginTableModel (

    var Username: String,
    var Address: String,
    var Email: String,
    var Phone: String,
    var Img: String

) {

    @PrimaryKey(autoGenerate = true)
    var Id: Int? = null

}