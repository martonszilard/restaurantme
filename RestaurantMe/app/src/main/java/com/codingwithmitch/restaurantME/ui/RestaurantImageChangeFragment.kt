package com.codingwithmitch.restaurantME.ui

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.codingwithmitch.restaurantME.R
import com.bumptech.glide.Glide
<<<<<<< HEAD:RestaurantMe/app/src/main/java/com/codingwithmitch/navigationcomponentsexample/ui/RestaurantImageChangeFragment.kt
import com.codingwithmitch.navigationcomponentsexample.R
import com.codingwithmitch.navigationcomponentsexample.model.FavoritizedRestaurantsTableModel
import com.codingwithmitch.navigationcomponentsexample.model.LoginViewModel
import com.codingwithmitch.navigationcomponentsexample.model.Supplier
=======
import com.codingwithmitch.restaurantME.model.ViewModel
import com.codingwithmitch.restaurantME.model.Supplier
>>>>>>> feature/FavoriteList:RestaurantMe/app/src/main/java/com/codingwithmitch/restaurantME/ui/RestaurantImageChangeFragment.kt
import kotlinx.android.synthetic.main.fragment_restaurant_image_change.*

class RestaurantImageChangeFragment : Fragment() {

    private val args: RestaurantDetailFragmentArgs by navArgs()
    lateinit var loginViewModel: ViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginViewModel = ViewModelProvider(this).get(ViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Glide.with(change_img)
            .load(Supplier.restaurants[args.position].image)
            .into(change_img)

        btn_pick_img.setOnClickListener {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if(context?.checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_DENIED) {
                    val permissions = arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    requestPermissions(permissions,
                        PERMISSION_CODE
                    )
                } else {
                    pickImageFromGallery()
                }
            } else {
                pickImageFromGallery()
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_restaurant_image_change, container,
            false)
    }

    private fun pickImageFromGallery() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent,
            IMG_PICK_CODE
        )
    }

    companion object {
        private val IMG_PICK_CODE = 1000
        private val PERMISSION_CODE = 1001
    }

    //handle permission request result
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when(requestCode) {
            PERMISSION_CODE -> {
                if( grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //permission from popup granted
                    pickImageFromGallery()
                } else {
                    Toast.makeText(context, "Permission denied!", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(resultCode == Activity.RESULT_OK && requestCode == IMG_PICK_CODE) {
            if(!Supplier.restaurants[args.position].insertedInDb) {
                Supplier.restaurants[args.position].insertedInDb = true
                loginViewModel.insertFavorite(requireContext(), Supplier.restaurants[args.position])
            } else {
                loginViewModel.updateFavorite(requireContext(),
                    Supplier.restaurants[args.position].idd, data?.data.toString(),
                    Supplier.restaurants[args.position].favorite)
            }
            Supplier.restaurants[args.position].hasImage = true
            Supplier.restaurants[args.position].image = data?.data.toString()
            fragmentManager?.popBackStack()
        }
    }
}