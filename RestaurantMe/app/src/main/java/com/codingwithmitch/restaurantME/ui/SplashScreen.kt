package com.codingwithmitch.restaurantME.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
<<<<<<< HEAD:RestaurantMe/app/src/main/java/com/codingwithmitch/navigationcomponentsexample/ui/SplashScreen.kt
import android.util.Log
import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import com.codingwithmitch.navigationcomponentsexample.Injection
import com.codingwithmitch.navigationcomponentsexample.R
import com.codingwithmitch.navigationcomponentsexample.model.FavoritizedRestaurantsTableModel
import com.codingwithmitch.navigationcomponentsexample.model.LoginViewModel
import com.codingwithmitch.navigationcomponentsexample.model.Restaurant
import com.codingwithmitch.navigationcomponentsexample.model.Supplier
=======
import com.codingwithmitch.restaurantME.R
>>>>>>> feature/FavoriteList:RestaurantMe/app/src/main/java/com/codingwithmitch/restaurantME/ui/SplashScreen.kt

class SplashScreen : AppCompatActivity() {

    lateinit var loginViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        val viewModel = ViewModelProvider(this, Injection.provideViewModelFactory())
            .get(RestaurantListViewModel::class.java)
        val query = savedInstanceState?.getString(RestaurantListFragment.LAST_SEARCH_QUERY) ?: RestaurantListFragment.DEFAULT_QUERY
        if (viewModel.restaurantResult.value == null) {
            viewModel.searchRepo(query)
        }
        viewModel.listScrolled(0, 0, 0)
        loginViewModel = ViewModelProvider(this).get(LoginViewModel::class.java)
        loginViewModel.getAllFavorites(this)?.observe(this, {
            for(i in it) {
                var rest: Restaurant = Restaurant(i.id, i.name, i.address, i.city, i.state, i.area,
                    i.postal_code, i.country, i.phone, i.lat, i.lng, i.price, i.image, i.reserve_url,
                i.mobile_reserve_url, i.favorite, i.hasImage, i.insertedInDb)
                Supplier.databaseFavorites.add(rest)
            }
        })

        supportActionBar?.hide()
        Handler().postDelayed({
            val intent = Intent(this@SplashScreen, MainActivity::class.java)
            startActivity(intent)
            finish()
        }, 3000)
    }
}
