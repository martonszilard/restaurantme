package com.codingwithmitch.restaurantME.ui

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.codingwithmitch.restaurantME.R
import com.bumptech.glide.Glide
import com.codingwithmitch.restaurantME.model.ViewModel
import kotlinx.android.synthetic.main.fragment_profile_login_register.*

class ProfileLoginRegisterFragment : Fragment() {

    lateinit var userName: String
    lateinit var userAddress: String
    lateinit var userEmail: String
    lateinit var userPhone: String
    lateinit var profileImg: String

    lateinit var viewModel: ViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile_login_register, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_register_change_pic.setOnClickListener {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if(context?.checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                    val permissions = arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    requestPermissions(permissions,
                        ProfileLoginRegisterFragment.PERMISSION_CODE
                    )
                } else {
                    pickImageFromGallery()
                }
            } else {
                pickImageFromGallery()
            }
        }

        btn_register.setOnClickListener {
            userName = register_input_name.text.toString().trim()
            userAddress = register_input_address.text.toString().trim()
            userEmail = register_input_email.text.toString().trim()
            userPhone = register_input_phone.text.toString().trim()
            profileImg = R.drawable.ic_favorite_red.toString()
            Glide.with(requireContext())
                .load("https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.iconfinder.com%2Ficons%2F2247649%2Favatar_my_profile_profile_user_user_profile_icon&psig=AOvVaw2v7-cAWKf3Oyfr9MbiL-hz&ust=1607625902503000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCOiby6rHwe0CFQAAAAAdAAAAABAD")
                .into(register_pic)

            if (userName.isEmpty() || userAddress.isEmpty() || userEmail.isEmpty()
                || userPhone.isEmpty() || userEmail.isEmpty()
            ) {
                register_input_name.error = "Please fill all spaces"
            } else {
                context?.let { it1 -> viewModel.insertData(it1, userName, userAddress, userEmail, userPhone, profileImg) }
            }
            Navigation.findNavController(view).navigate(R.id.action_profileLoginRegisterFragment_to_profileFragment)
        }
    }

    private fun pickImageFromGallery() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent,
            IMG_PICK_CODE
        )
    }

    companion object {
        private val IMG_PICK_CODE = 1000
        private val PERMISSION_CODE = 1001
    }

    //handle permission request result
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when(requestCode) {
            PERMISSION_CODE -> {
                if( grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //permission from popup granted
                    pickImageFromGallery()
                } else {
                    Toast.makeText(context, "Permission denied!", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(resultCode == Activity.RESULT_OK && requestCode == IMG_PICK_CODE) {
            profileImg = data?.data.toString()
            viewModel.updateUser(requireContext(), userName, userEmail, userAddress, userPhone, profileImg)
            Glide.with(requireContext())
                .load(data?.data)
                .into(register_pic)
        }
    }
}