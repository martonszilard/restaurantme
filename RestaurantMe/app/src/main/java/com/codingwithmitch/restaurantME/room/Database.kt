package com.codingwithmitch.restaurantME.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.codingwithmitch.restaurantME.model.FavoritizedRestaurantsTableModel
import com.codingwithmitch.restaurantME.model.LoginTableModel

@Database(
    entities = [
        LoginTableModel::class,
        FavoritizedRestaurantsTableModel::class
               ],
    version = 8, exportSchema = false
)
abstract class Database : RoomDatabase() {

    abstract fun loginDao() : DAOAccess

    companion object {

        @Volatile
        private var INSTANCE: com.codingwithmitch.restaurantME.room.Database? = null

        fun getDataseClient(context: Context): com.codingwithmitch.restaurantME.room.Database {

            if (INSTANCE != null) return INSTANCE!!

            synchronized(this) {

                INSTANCE = Room
                    .databaseBuilder(context, com.codingwithmitch.restaurantME.room.Database::class.java, "LOGIN_DATABASE")
                    .fallbackToDestructiveMigration()
                    .build()

                return INSTANCE!!
            }
        }
    }
}