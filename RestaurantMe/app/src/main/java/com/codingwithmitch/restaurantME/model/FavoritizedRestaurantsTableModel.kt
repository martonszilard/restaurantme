package com.codingwithmitch.restaurantME.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "FavoriteRestaurants")
data class FavoritizedRestaurantsTableModel (

    @PrimaryKey()
    var id: Long,
    val name: String,
    val address: String,
    val city: String,
    val state: String,
    val area: String,
    val postal_code: Int,
    val country: String,
    val phone: String,
    val lat: Float,
    val lng: Float,
    val price: Float,
    var image: String,
    val reserve_url: String,
    val mobile_reserve_url: String,
    var favorite: Boolean = false,
    var hasImage: Boolean = false,
    var insertedInDb: Boolean = false

)