package com.codingwithmitch.restaurantME.room

import androidx.lifecycle.LiveData
import androidx.room.*
<<<<<<< HEAD:RestaurantMe/app/src/main/java/com/codingwithmitch/navigationcomponentsexample/room/DAOAcces.kt
import com.codingwithmitch.navigationcomponentsexample.model.FavoritizedRestaurantsTableModel
import com.codingwithmitch.navigationcomponentsexample.model.LoginTableModel
import com.codingwithmitch.navigationcomponentsexample.model.Restaurant
=======
import com.codingwithmitch.restaurantME.model.FavoritizedRestaurantsTableModel
import com.codingwithmitch.restaurantME.model.LoginTableModel
>>>>>>> feature/FavoriteList:RestaurantMe/app/src/main/java/com/codingwithmitch/restaurantME/room/DAOAcces.kt

@Dao
interface DAOAccess {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun InsertData(loginTableModel: LoginTableModel)

    @Query("UPDATE users SET username = :name, address = :address, email = :email,  phone = :phone, img = :img")
    fun updateUser(name: String, email: String, address: String, phone: String, img: String)

    @Query("SELECT * FROM users")
    fun getLoginDetails() : LiveData<LoginTableModel>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun InsertFavorite(favoritizedRestaurantsTableModel: FavoritizedRestaurantsTableModel)

    @Query("UPDATE FavoriteRestaurants SET favorite = :favorited WHERE id == :id")
    fun updateFavorites(id: Long, favorited: Boolean)

    @Query("UPDATE FavoriteRestaurants SET image = :image WHERE id == :id")
    fun updateFavorites(id: Long, image: String)

    @Query("SELECT * FROM FavoriteRestaurants WHERE id LIKE :restID")
    fun getFavoritesDetails(restID: Long) : LiveData<FavoritizedRestaurantsTableModel>

<<<<<<< HEAD:RestaurantMe/app/src/main/java/com/codingwithmitch/navigationcomponentsexample/room/DAOAcces.kt
    @Query("SELECT * FROM FavoriteRestaurants WHERE favorite = 1")
=======
    @Query("SELECT * FROM FavoriteRestaurants WHERE favorited = 1")
>>>>>>> feature/FavoriteList:RestaurantMe/app/src/main/java/com/codingwithmitch/restaurantME/room/DAOAcces.kt
    fun getAllFavorites() : LiveData<List<FavoritizedRestaurantsTableModel>>

    @Query("UPDATE FavoriteRestaurants SET favorite = 0 WHERE id LIKE :restID")
    fun removeFavoritesDetails(restID: Long)

    @Query("DELETE FROM FavoriteRestaurants WHERE favorite = 0 AND Image = 'NULL'")
    fun removeUnfavoritized()

}