package com.codingwithmitch.restaurantME.data

import android.content.Context
import androidx.lifecycle.LiveData
<<<<<<< HEAD:RestaurantMe/app/src/main/java/com/codingwithmitch/navigationcomponentsexample/data/LoginRepository.kt
import com.codingwithmitch.navigationcomponentsexample.model.FavoritizedRestaurantsTableModel
import com.codingwithmitch.navigationcomponentsexample.model.LoginTableModel
import com.codingwithmitch.navigationcomponentsexample.model.Restaurant
import com.codingwithmitch.navigationcomponentsexample.room.LoginDatabase
=======
import com.codingwithmitch.restaurantME.model.FavoritizedRestaurantsTableModel
import com.codingwithmitch.restaurantME.model.LoginTableModel
import com.codingwithmitch.restaurantME.room.Database
>>>>>>> feature/FavoriteList:RestaurantMe/app/src/main/java/com/codingwithmitch/restaurantME/data/DatabaseRepository.kt
import kotlinx.coroutines.*

class DatabaseRepository {

    companion object {

        var loginDatabase: Database? = null

        var loginTableModel: LiveData<LoginTableModel>? = null
        var favoriteTableModel: LiveData<FavoritizedRestaurantsTableModel>? = null

        fun initializeLoginDB(context: Context) : Database {
            return Database.getDataseClient(context)
        }

        fun insertData(context: Context, username: String, address: String, email: String,
        phone: String, image: String) {

            loginDatabase = initializeLoginDB(context)

            CoroutineScope(Dispatchers.IO).launch {
                val loginDetails = LoginTableModel(username, address, email, phone, image)
                loginDatabase!!.loginDao().InsertData(loginDetails)
            }

        }

        fun updateUser(context: Context, name: String, email: String, address: String, phone: String, img: String) {
            loginDatabase = initializeLoginDB(context)

            CoroutineScope(Dispatchers.IO).launch {
                loginDatabase!!.loginDao().updateUser(name, email, address, phone, img)
            }
        }

        fun getLoginDetails(context: Context) : LiveData<LoginTableModel>? {

            loginDatabase = initializeLoginDB(context)

            loginTableModel = loginDatabase!!.loginDao().getLoginDetails()

            return loginTableModel
        }

        fun insertFavorite(context: Context, rest: Restaurant) {
            loginDatabase = initializeLoginDB(context)

            CoroutineScope(Dispatchers.IO).launch {
                val restaurant = FavoritizedRestaurantsTableModel(rest.idd, rest.name, rest.address,
                    rest.city, rest.state, rest.area, rest.postal_code.toInt(), rest.country, rest.phone,
                    rest.lat.toFloat(), rest.lng.toFloat(), rest.price.toFloat(), rest.image, rest.reserve_url,
                    rest.mobile_reserve_url, rest.favorite, rest.hasImage, rest.insertedInDb)
                loginDatabase!!.loginDao().InsertFavorite(restaurant)
            }

        }

        fun updateFavorite(context: Context, id: Long, favorited: Boolean) {
            loginDatabase = initializeLoginDB(context)

            CoroutineScope(Dispatchers.IO).launch {
                loginDatabase!!.loginDao().updateFavorites(id, favorited)
            }

        }

        fun updateFavorite(context: Context, id: Long, image: String) {
            loginDatabase = initializeLoginDB(context)

            CoroutineScope(Dispatchers.IO).launch {
                loginDatabase!!.loginDao().updateFavorites(id, image)
            }

        }

        fun getAllFavorites(context: Context) : LiveData<List<FavoritizedRestaurantsTableModel>> {
            loginDatabase = initializeLoginDB(context)
            val result = loginDatabase!!.loginDao().getAllFavorites()
            return result
        }

        fun getFavoritesDetails(context: Context, restID: Long) : LiveData<FavoritizedRestaurantsTableModel>? {

            loginDatabase = initializeLoginDB(context)

            favoriteTableModel = loginDatabase!!.loginDao().getFavoritesDetails(restID)

            return favoriteTableModel
        }

        fun removeFavoritesDetails(context: Context, restID: Long) {

            loginDatabase = initializeLoginDB(context)

            loginDatabase!!.loginDao().removeFavoritesDetails(restID)
        }

        fun removeUnfavoritized(context: Context) {

            loginDatabase = initializeLoginDB(context)

            GlobalScope.launch{
                delay(3000)
                loginDatabase!!.loginDao().removeUnfavoritized()
            }
        }
    }
}