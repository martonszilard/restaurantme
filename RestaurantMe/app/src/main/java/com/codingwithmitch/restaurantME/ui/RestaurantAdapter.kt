package com.codingwithmitch.restaurantME.ui

import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.codingwithmitch.restaurantME.model.ViewModel
import com.codingwithmitch.restaurantME.model.Restaurant


class RestaurantAdapter(var lifecycleOwner: LifecycleOwner, var viewModel: ViewModel) : ListAdapter<Restaurant, androidx.recyclerview.widget.RecyclerView.ViewHolder>(REPO_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): androidx.recyclerview.widget.RecyclerView.ViewHolder {
        return RestaurantListViewHolder.create(parent, lifecycleOwner, viewModel)
    }

    override fun onBindViewHolder(holder: androidx.recyclerview.widget.RecyclerView.ViewHolder, position: Int) {
        val restaurantItem = getItem(position)
        if(restaurantItem != null) {
            (holder as RestaurantListViewHolder).bind(restaurantItem)
        }
    }

    companion object {
        private val REPO_COMPARATOR = object : DiffUtil.ItemCallback<Restaurant>() {
            override fun areItemsTheSame(oldItem: Restaurant, newItem: Restaurant): Boolean =
                oldItem.idd == newItem.idd

            override fun areContentsTheSame(oldItem: Restaurant, newItem: Restaurant): Boolean =
                oldItem.idd == newItem.idd
        }
    }
}