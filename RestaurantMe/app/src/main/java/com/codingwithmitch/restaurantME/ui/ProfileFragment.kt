package com.codingwithmitch.restaurantME.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.codingwithmitch.restaurantME.R
import com.codingwithmitch.restaurantME.model.ViewModel
import kotlinx.android.synthetic.main.fragment_profile.*

class ProfileFragment : Fragment() {

    lateinit var viewModel: ViewModel
    private lateinit var list: ListView
    private lateinit var arrayAdapter: ArrayAdapter<String>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(ViewModel::class.java)
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        list = view.findViewById(R.id.favoriteList)
        arrayAdapter = ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1)
        viewModel.getAllFavorites(requireContext())?.observe(viewLifecycleOwner, {
            if(it != null) {
                for(i in it) {
                    arrayAdapter.add(i.restaurantName)
                }
            }
            list.adapter = arrayAdapter
        })


        viewModel.getLoginDetails(requireContext())!!.observe(viewLifecycleOwner, {
            if (it != null) {
                profile_name.text = it.Username
                profile_address.text = it.Address
                profile_email.text = it.Email
                profile_phone_number.text = it.Phone
                Glide.with(this)
                    .load(it.Img)
                    .circleCrop()
                    .into(profile_img)
            }
        })

        btn_edit_profile.setOnClickListener{
            val action = ProfileFragmentDirections.actionProfileFragmentToProfileEditFragment(
                profile_name.text.toString(),
                profile_address.text.toString(),
                profile_phone_number.text.toString(),
                profile_email.text.toString(),
                profile_img.toString()
            )
            Navigation.findNavController(view).navigate(action)
        }
    }
}