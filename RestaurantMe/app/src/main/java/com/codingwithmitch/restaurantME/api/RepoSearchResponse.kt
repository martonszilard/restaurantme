/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.codingwithmitch.restaurantME.api

import com.codingwithmitch.restaurantME.model.Restaurant
import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Data class to hold repo responses from searchRepo API calls.
 */
data class RepoSearchResponse(
    @SerializedName("total_entries") val total: Int = 0,
    @SerializedName("page") val current_page: Int = 0,
    @SerializedName("per_page") val per_page: Int = 0,
    @SerializedName("restaurants") val items: List<Restaurant> = emptyList(),
    val nextPage: Int? = null
)
