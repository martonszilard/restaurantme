/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.codingwithmitch.restaurantME.ui

import android.util.Log
import android.util.Log.INFO
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.*
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
<<<<<<< HEAD:RestaurantMe/app/src/main/java/com/codingwithmitch/navigationcomponentsexample/ui/RestaurantListViewHolder.kt
import com.codingwithmitch.navigationcomponentsexample.R
import com.codingwithmitch.navigationcomponentsexample.model.*
=======
import com.codingwithmitch.restaurantME.R
import com.codingwithmitch.restaurantME.model.ViewModel
import com.codingwithmitch.restaurantME.model.Restaurant
import com.codingwithmitch.restaurantME.model.Supplier
>>>>>>> feature/FavoriteList:RestaurantMe/app/src/main/java/com/codingwithmitch/restaurantME/ui/RestaurantListViewHolder.kt
import kotlinx.android.synthetic.main.card_item.view.*
import okhttp3.internal.platform.Platform.Companion.INFO
import java.util.logging.Level.INFO

/**
 * View Holder for a [loadableRestaurant] RecyclerView list item.
 */
<<<<<<< HEAD:RestaurantMe/app/src/main/java/com/codingwithmitch/navigationcomponentsexample/ui/RestaurantListViewHolder.kt
class RestaurantListViewHolder(view: View, val viewModel:  LoginViewModel, val lifecycleOwner: LifecycleOwner) : RecyclerView.ViewHolder(view) {
=======
class RestaurantListViewHolder(view: View, val lifecycleOwner: LifecycleOwner, val viewModel: ViewModel) : RecyclerView.ViewHolder(view) {
>>>>>>> feature/FavoriteList:RestaurantMe/app/src/main/java/com/codingwithmitch/restaurantME/ui/RestaurantListViewHolder.kt

    private val name: TextView = view.findViewById(R.id.rest_name)
    private val address: TextView = view.findViewById(R.id.rest_addr)
    private val image: ImageView = view.findViewById(R.id.rest_img)
    private val price: TextView = view.findViewById(R.id.rest_price)
    private val btn_fav: Button = view.findViewById<Button>(R.id.btn_fav)
<<<<<<< HEAD:RestaurantMe/app/src/main/java/com/codingwithmitch/navigationcomponentsexample/ui/RestaurantListViewHolder.kt

=======
    
>>>>>>> feature/FavoriteList:RestaurantMe/app/src/main/java/com/codingwithmitch/restaurantME/ui/RestaurantListViewHolder.kt
    private val context = view.context
    private var loadableRestaurant: Restaurant? = null

    init {
        itemView.setOnClickListener {
            val action =
                RestaurantListFragmentDirections.actionRestaurantListFragmentToRestaurantDetailFragment(
                    adapterPosition
                )
            Navigation.findNavController(itemView).navigate(action)
        }

        itemView.btn_fav.setOnClickListener {
            if (Supplier.restaurants[adapterPosition].favorite) {
                Supplier.restaurants[adapterPosition].favorite = false
                itemView.btn_fav.setBackgroundResource(R.drawable.ic_favorite_black)

<<<<<<< HEAD:RestaurantMe/app/src/main/java/com/codingwithmitch/navigationcomponentsexample/ui/RestaurantListViewHolder.kt
                 viewModel.updateFavorite(
=======
                viewModel.updateFavorite(
>>>>>>> feature/FavoriteList:RestaurantMe/app/src/main/java/com/codingwithmitch/restaurantME/ui/RestaurantListViewHolder.kt
                    view.context,
                    loadableRestaurant!!.idd,
                    "NULL",
                    false
                )
<<<<<<< HEAD:RestaurantMe/app/src/main/java/com/codingwithmitch/navigationcomponentsexample/ui/RestaurantListViewHolder.kt
                 viewModel.removeUnfavoritized(context)
=======
                viewModel.removeUnfavoritized(context)
>>>>>>> feature/FavoriteList:RestaurantMe/app/src/main/java/com/codingwithmitch/restaurantME/ui/RestaurantListViewHolder.kt
                Supplier.restaurants[adapterPosition].insertedInDb = false
                Log.i("Remove rest from fav:", Supplier.restaurants[adapterPosition].toString())

            } else {
                if(!Supplier.restaurants[adapterPosition].hasImage){
                    Supplier.restaurants[adapterPosition].insertedInDb = false
                }
                Supplier.restaurants[adapterPosition].favorite = true
                itemView.btn_fav.setBackgroundResource(R.drawable.ic_favorite_red)

                if(!Supplier.restaurants[adapterPosition].insertedInDb) {
<<<<<<< HEAD:RestaurantMe/app/src/main/java/com/codingwithmitch/navigationcomponentsexample/ui/RestaurantListViewHolder.kt
                    val toInsert = loadableRestaurant
                    toInsert?.favorite = true
                    if (toInsert != null) {
                         viewModel.insertFavorite(
                            view.context,
                            toInsert
                        )
                    }
                    Supplier.restaurants[adapterPosition].insertedInDb = true

                } else {
                     viewModel.updateFavorite(
=======
                    viewModel.insertFavorite(
                        view.context,
                        loadableRestaurant!!.idd,
                        itemView.rest_name.text.toString(),
                        "NULL",
                        true
                    )
                    Supplier.restaurants[adapterPosition].insertedInDb = true

                } else {
                    viewModel.updateFavorite(
>>>>>>> feature/FavoriteList:RestaurantMe/app/src/main/java/com/codingwithmitch/restaurantME/ui/RestaurantListViewHolder.kt
                        view.context,
                        loadableRestaurant!!.idd,
                        "NULL",
                        true
                    )
                }
                Log.i("Favoritize restaurant: ", Supplier.restaurants[adapterPosition].toString())
            }
        }
    }
<<<<<<< HEAD:RestaurantMe/app/src/main/java/com/codingwithmitch/navigationcomponentsexample/ui/RestaurantListViewHolder.kt

=======
    
>>>>>>> feature/FavoriteList:RestaurantMe/app/src/main/java/com/codingwithmitch/restaurantME/ui/RestaurantListViewHolder.kt
    fun bind(restaurant: Restaurant?) {
        if (restaurant == null) {
            val resources = itemView.resources
            name.text = resources.getString(R.string.loading)
            address.visibility = View.GONE
            price.text = resources.getString(R.string.unknown)
        } else {
            showLoadableRestaurantData(restaurant)
        }
    }

    private fun showLoadableRestaurantData(restaurant: Restaurant) {
        Log.i("Loading restaurant", restaurant.toString())
        loadableRestaurant = restaurant
        name.text = restaurant.name
        address.text = restaurant.address
        price.text = restaurant.price.toString()
        viewModel.getFavorites(context, restaurant.idd)?.observe(lifecycleOwner, {
            if (it != null) {
                if (it.image != "NULL") {
                    Glide.with(context)
                        .load(it.image)
                        .centerCrop()
                        .into(image)
                    Supplier.restaurants[Supplier.restaurants.indexOf(Supplier.restaurants.find { it.idd == restaurant.idd })].image =
                        it.image
                    Supplier.restaurants[Supplier.restaurants.indexOf(Supplier.restaurants.find { it.idd == restaurant.idd })].hasImage =
                        true
                } else {
                    Glide.with(context)
                        .load(restaurant.image)
                        .centerCrop()
                        .into(image)
                    btn_fav.setBackgroundResource(R.drawable.ic_favorite_black)
                }

                if (it.favorite) {
                    Supplier.restaurants[Supplier.restaurants.indexOf(Supplier.restaurants.find { it.idd == restaurant.idd })].favorite =
                        true
                    btn_fav.setBackgroundResource(R.drawable.ic_favorite_red)
                }
                Supplier.restaurants[Supplier.restaurants.indexOf(Supplier.restaurants.find { it.idd == restaurant.idd })].insertedInDb =
                    true
            } else {
                Glide.with(context)
                    .load(restaurant.image)
                    .centerCrop()
                    .into(image)
                btn_fav.setBackgroundResource(R.drawable.ic_favorite_black)
            }
        })
    }

    companion object {
<<<<<<< HEAD:RestaurantMe/app/src/main/java/com/codingwithmitch/navigationcomponentsexample/ui/RestaurantListViewHolder.kt
        fun create(parent: ViewGroup, viewModel:  LoginViewModel, lifecycleOwner: LifecycleOwner): RestaurantListViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.card_item, parent, false)
            return RestaurantListViewHolder(view, viewModel, lifecycleOwner)
=======
        fun create(parent: ViewGroup, lifecycleOwner: LifecycleOwner, viewModel: ViewModel): RestaurantListViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.card_item, parent, false)
            return RestaurantListViewHolder(view, lifecycleOwner, viewModel)
>>>>>>> feature/FavoriteList:RestaurantMe/app/src/main/java/com/codingwithmitch/restaurantME/ui/RestaurantListViewHolder.kt
        }
    }
}
