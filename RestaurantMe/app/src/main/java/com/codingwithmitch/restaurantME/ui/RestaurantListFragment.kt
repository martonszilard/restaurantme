package com.codingwithmitch.restaurantME.ui

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.codingwithmitch.restaurantME.databinding.FragmentRestaurantListBinding
import com.codingwithmitch.restaurantME.R
import com.bumptech.glide.Glide
<<<<<<< HEAD:RestaurantMe/app/src/main/java/com/codingwithmitch/navigationcomponentsexample/ui/RestaurantListFragment.kt
import com.bumptech.glide.load.resource.bitmap.BitmapImageDecoderResourceDecoder
import com.codingwithmitch.navigationcomponentsexample.Injection
import com.codingwithmitch.navigationcomponentsexample.R
import com.codingwithmitch.navigationcomponentsexample.databinding.FragmentRestaurantListBinding
import com.codingwithmitch.navigationcomponentsexample.model.LoginViewModel
import com.codingwithmitch.navigationcomponentsexample.model.RepoSearchResult
import com.codingwithmitch.navigationcomponentsexample.model.Restaurant
import com.codingwithmitch.navigationcomponentsexample.model.Supplier
import kotlinx.android.synthetic.main.card_item.view.*
import kotlinx.android.synthetic.main.fragment_restaurant_list.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.asFlow
import java.io.File
import java.util.*
=======
import com.codingwithmitch.restaurantME.Injection
import com.codingwithmitch.restaurantME.model.ViewModel
import com.codingwithmitch.restaurantME.model.RepoSearchResult
import kotlinx.android.synthetic.main.fragment_restaurant_list.*
>>>>>>> feature/FavoriteList:RestaurantMe/app/src/main/java/com/codingwithmitch/restaurantME/ui/RestaurantListFragment.kt

class RestaurantListFragment : Fragment() {

    private lateinit var bind: FragmentRestaurantListBinding
    private lateinit var viewModel:RestaurantListViewModel
    private lateinit var adapter: RestaurantAdapter
<<<<<<< HEAD:RestaurantMe/app/src/main/java/com/codingwithmitch/navigationcomponentsexample/ui/RestaurantListFragment.kt
    private var registered = false
    lateinit var loginViewModel: LoginViewModel
=======
    private lateinit var DbViewModel: ViewModel
>>>>>>> feature/FavoriteList:RestaurantMe/app/src/main/java/com/codingwithmitch/restaurantME/ui/RestaurantListFragment.kt
    private var viewGroup: ViewGroup? = null
    private var registered = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
<<<<<<< HEAD:RestaurantMe/app/src/main/java/com/codingwithmitch/navigationcomponentsexample/ui/RestaurantListFragment.kt
        loginViewModel = ViewModelProvider(this).get(LoginViewModel::class.java)
=======
        // Inflate the layout for this fragment
        DbViewModel = ViewModelProvider(this).get(ViewModel::class.java)
>>>>>>> feature/FavoriteList:RestaurantMe/app/src/main/java/com/codingwithmitch/restaurantME/ui/RestaurantListFragment.kt
        bind = FragmentRestaurantListBinding.inflate(layoutInflater)
        viewGroup = container
        viewModel = ViewModelProvider(this, Injection.provideViewModelFactory())
            .get(RestaurantListViewModel::class.java)
<<<<<<< HEAD:RestaurantMe/app/src/main/java/com/codingwithmitch/navigationcomponentsexample/ui/RestaurantListFragment.kt
=======

        setupScrollListener()

        adapter = RestaurantAdapter(viewLifecycleOwner, DbViewModel)
        initAdapter()

        val query = savedInstanceState?.getString(LAST_SEARCH_QUERY) ?: DEFAULT_QUERY
        if (viewModel.restaurantResult.value == null) {
            viewModel.searchRepo(query)
        }
        initSearch(query)

>>>>>>> feature/FavoriteList:RestaurantMe/app/src/main/java/com/codingwithmitch/restaurantME/ui/RestaurantListFragment.kt
        return bind.root
    }

    @RequiresApi(Build.VERSION_CODES.P)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        DbViewModel.getLoginDetails(this.requireContext())!!.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                this.registered = true
                Glide.with(this)
                    .load(it.Img)
                    .circleCrop()
                    .into(btn_profile)
            }
        })

        if(!this::adapter.isInitialized) {
            initAdapter()
            setupScrollListener()

            /*val query = savedInstanceState?.getString(LAST_SEARCH_QUERY) ?: DEFAULT_QUERY
            if (viewModel.restaurantResult.value == null) {
                viewModel.searchRepo(query)
            }*/

            //initSearch(query)
        } else {
            bind.recyclerView.adapter = adapter
            Log.i("bind.adapter = adapter", adapter.toString())
        }

        btn_profile.setOnClickListener {
            if(!registered) {
                Navigation.findNavController(view)
                    .navigate(R.id.action_restaurantListFragment_to_profileLoginRegisterFragment)

            } else {
                Navigation.findNavController(view)
                    .navigate(R.id.action_restaurantListFragment_to_profileFragment)
            }
        }
<<<<<<< HEAD:RestaurantMe/app/src/main/java/com/codingwithmitch/navigationcomponentsexample/ui/RestaurantListFragment.kt

        favBox.setOnCheckedChangeListener {
            buttonView, isChecked ->
            if(isChecked) {
                Log.i("Show favorites:", Supplier.databaseFavorites.toString())
                adapter.currentList.clear()
                Log.i("adapter after clear", adapter.currentList.toString())
                recyclerView.adapter?.notifyDataSetChanged()
                loginViewModel.getAllFavorites(requireContext()).observe(viewLifecycleOwner, {
                    val list: MutableList<Restaurant> = mutableListOf()
                    for(i in it) {
                        list.add(Restaurant(i.id, i.name, i.address, i.city, i.state, i.area,
                            i.postal_code, i.country, i.phone, i.lat, i.lng, i.price, i.image, i.reserve_url,
                            i.mobile_reserve_url, i.favorite, i.hasImage, i.insertedInDb))
                    }
                    adapter.submitList(list)
                })
            } else {
                Log.i("Show all restaurants:", Supplier.restaurants.toString())
                adapter.submitList(Collections.emptyList())
                recyclerView.adapter?.notifyDataSetChanged()
                viewModel.restaurantResult.observe(viewLifecycleOwner) { result ->
                    when (result) {
                        is RepoSearchResult.Success -> {
                            adapter.submitList((result.data))
                        }
                        is RepoSearchResult.Error -> {
                            Toast.makeText(context, "Woops $result.message}", Toast.LENGTH_LONG).show()
                        }
                    }
                }
            }
        }
=======
>>>>>>> feature/FavoriteList:RestaurantMe/app/src/main/java/com/codingwithmitch/restaurantME/ui/RestaurantListFragment.kt
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(LAST_SEARCH_QUERY, R.id.search_restaurant.toString())
    }

    private fun initAdapter() {
<<<<<<< HEAD:RestaurantMe/app/src/main/java/com/codingwithmitch/navigationcomponentsexample/ui/RestaurantListFragment.kt
        adapter = RestaurantAdapter(loginViewModel, this, Supplier.restaurants)
        bind.recyclerView.adapter = adapter

        Log.i("Init adapter", adapter.currentList.toString())

=======
        Log.i("aaaa", "a")
        bind.recyclerView.adapter = adapter
>>>>>>> feature/FavoriteList:RestaurantMe/app/src/main/java/com/codingwithmitch/restaurantME/ui/RestaurantListFragment.kt
        viewModel.restaurantResult.observe(viewLifecycleOwner) { result ->
            when (result) {
                is RepoSearchResult.Success -> {
                    adapter.submitList((result.data))
<<<<<<< HEAD:RestaurantMe/app/src/main/java/com/codingwithmitch/navigationcomponentsexample/ui/RestaurantListFragment.kt
=======
                    adapter.notifyDataSetChanged()
>>>>>>> feature/FavoriteList:RestaurantMe/app/src/main/java/com/codingwithmitch/restaurantME/ui/RestaurantListFragment.kt
                }
                is RepoSearchResult.Error -> {
                    Toast.makeText(context, "Woops $result.message}", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun initSearch(query: String) {
        bind.searchRestaurant.setText(query)
        bind.searchRestaurant.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_GO) {
                updateRepoListFromInput()
                true
            } else {
                false
            }
        }
        bind.searchRestaurant.setOnKeyListener { _, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                updateRepoListFromInput()
                true
            } else {
                false
            }
        }
    }

    private fun updateRepoListFromInput() {
        bind.searchRestaurant.text.trim().let {
            if (it.isNotEmpty()) {
                bind.recyclerView.scrollToPosition(0)
                viewModel.searchRepo(it.toString())
            }
        }
    }

    private fun setupScrollListener() {
        val layoutManager = bind.recyclerView.layoutManager as LinearLayoutManager
        bind.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (!favBox.isChecked) {
                    super.onScrolled(recyclerView, dx, dy)
                    val totalItemCount = layoutManager.itemCount
                    val visibleItemCount = layoutManager.childCount
                    val lastVisibleItem = layoutManager.findLastVisibleItemPosition()
                    viewModel.listScrolled(visibleItemCount, lastVisibleItem, totalItemCount)
                }
            }
        })
    }

    companion object {
<<<<<<< HEAD:RestaurantMe/app/src/main/java/com/codingwithmitch/navigationcomponentsexample/ui/RestaurantListFragment.kt
        const val LAST_SEARCH_QUERY: String = "last_search_query"
        const val DEFAULT_QUERY = "Chicago"
=======
        private const val LAST_SEARCH_QUERY: String = "last_search_query"
        private const val DEFAULT_QUERY = "Dallas"
>>>>>>> feature/FavoriteList:RestaurantMe/app/src/main/java/com/codingwithmitch/restaurantME/ui/RestaurantListFragment.kt
    }
}