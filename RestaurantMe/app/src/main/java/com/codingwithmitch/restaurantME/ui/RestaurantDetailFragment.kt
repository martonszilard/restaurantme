package com.codingwithmitch.restaurantME.ui

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import com.codingwithmitch.restaurantME.R
import com.bumptech.glide.Glide
import com.codingwithmitch.restaurantME.model.Supplier
import kotlinx.android.synthetic.main.fragment_restaurant_detail.*


class RestaurantDetailFragment : Fragment() {

    private val args: RestaurantDetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_restaurant_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadData()

        btn_go_map.setOnClickListener {
            Log.i("in description", "${Supplier.restaurants[args.position].lat.toFloat().toString()}, " +
                    "${Supplier.restaurants[args.position].lng.toFloat().toString()} ${args.position}")
            val action = RestaurantDetailFragmentDirections.actionRestaurantDetailFragmentToRestaurantMapsFragment(
                Supplier.restaurants[args.position].lng.toFloat(),
                Supplier.restaurants[args.position].lat.toFloat(),
                Supplier.restaurants[args.position].name.toString()
            )
            Navigation.findNavController(view).navigate(action)

        }

        btn_add_remove_img.setOnClickListener{
            val action =
                RestaurantDetailFragmentDirections.actionRestaurantDetailFragmentToRestaurantImageChangeFragment(
                    args.position
                )
            Navigation.findNavController(view).navigate(action)
        }
    }

    fun loadData() {
        detail_rest_name.text = Supplier.restaurants[args.position].name
        detail_rest_addr.text = Supplier.restaurants[args.position].address
        Glide.with(this)
            .load(Supplier.restaurants[args.position].image)
            .placeholder(R.drawable.ic_restaurant_background)
            .into(detail_rest_img)
        detail_city.text = Supplier.restaurants[args.position].city
        detail_state.text = Supplier.restaurants[args.position].state
        detail_area.text = Supplier.restaurants[args.position].area
        detail_postal_code.text = Supplier.restaurants[args.position].postal_code.toString()
        detail_country.text = Supplier.restaurants[args.position].country
        detail_phone_url.text = Supplier.restaurants[args.position].phone
        detail_rest_price.text = Supplier.restaurants[args.position].price.toString()
        detail_reserve_url.text = Supplier.restaurants[args.position].reserve_url
        detail_mobile_reserve_url.text = Supplier.restaurants[args.position].mobile_reserve_url
    }
}