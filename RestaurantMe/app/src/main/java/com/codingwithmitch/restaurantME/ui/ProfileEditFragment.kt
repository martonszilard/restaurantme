package com.codingwithmitch.restaurantME.ui

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.codingwithmitch.restaurantME.R
import com.bumptech.glide.Glide
import com.codingwithmitch.restaurantME.model.ViewModel
import kotlinx.android.synthetic.main.fragment_profile_edit.*

class ProfileEditFragment : Fragment() {

    lateinit var loginViewModel: ViewModel
    private val args: ProfileEditFragmentArgs by navArgs()
    lateinit var imagePath: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        loginViewModel = ViewModelProvider(this).get(ViewModel::class.java)
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile_edit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        loginViewModel.getLoginDetails(requireContext())?.observe(viewLifecycleOwner, {
            imagePath = it.Img
            Glide.with(requireContext())
                .load(Uri.parse("${it.Img}"))
                .into(profile_edit_img)
        })

        input_change_name.setText(args.name)
        input_change_address.setText(args.address)
        input_change_email.setText(args.address)
        input_change_phone.setText(args.address)

        btn_save_profile.setOnClickListener{
            loginViewModel.updateUser(requireContext(), input_change_name.text.toString(),
                input_change_email.text.toString(), input_change_address.text.toString(),
                input_change_phone.text.toString(), imagePath)
            fragmentManager?.popBackStack()
        }

        btn_change_profile_img.setOnClickListener {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if(context?.checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                    val permissions = arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    requestPermissions(permissions,
                        PERMISSION_CODE
                    )
                } else {
                    pickImageFromGallery()
                }
            } else {
                pickImageFromGallery()
            }
        }
    }

    private fun pickImageFromGallery() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent,
            IMG_PICK_CODE
        )
    }

    companion object {
        private val IMG_PICK_CODE = 1000
        private val PERMISSION_CODE = 1001
    }

    //handle permission request result
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when(requestCode) {
            PERMISSION_CODE -> {
                if( grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //permission from popup granted
                    pickImageFromGallery()
                } else {
                    Toast.makeText(context, "Permission denied!", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(resultCode == Activity.RESULT_OK && requestCode == IMG_PICK_CODE) {
            imagePath = data?.data.toString()
            Glide.with(requireContext())
                .load(data?.data)
                .into(profile_edit_img)
        }
    }
}
