package com.codingwithmitch.restaurantME.model

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.codingwithmitch.restaurantME.data.DatabaseRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class ViewModel(application: Application) : AndroidViewModel(application) {

    private var liveDataLogin: LiveData<LoginTableModel>? = null
    private var liveDataFavorite: LiveData<FavoritizedRestaurantsTableModel>? = null

    fun insertData(context: Context, username: String, address: String, email: String,
    phone: String, image: String) {
        DatabaseRepository.insertData(context, username, address, email, phone, image)
    }

    fun updateUser(context: Context, name: String, email: String, address: String, phone: String, img: String) {
        DatabaseRepository.updateUser(context, name, email, address, phone, img)
    }

    fun getLoginDetails(context: Context) : LiveData<LoginTableModel>? {
        liveDataLogin = DatabaseRepository.getLoginDetails(context)
        return liveDataLogin
    }

<<<<<<< HEAD:RestaurantMe/app/src/main/java/com/codingwithmitch/navigationcomponentsexample/model/LoginViewModel.kt
    fun insertFavorite(context: Context, restaurant: Restaurant) {
        LoginRepository.insertFavorite(context, restaurant)
=======
    fun insertFavorite(context: Context, id: Long, name: String, image: String, favorited: Boolean) {
        DatabaseRepository.insertFavorite(context, id, name, image, favorited)
>>>>>>> feature/FavoriteList:RestaurantMe/app/src/main/java/com/codingwithmitch/restaurantME/model/ViewModel.kt
    }

    fun updateFavorite(context: Context, id: Long, image: String, favorited: Boolean) {
        if(image != "NULL") {
            DatabaseRepository.updateFavorite(context, id, image)
        } else {
            DatabaseRepository.updateFavorite(context, id, favorited)
        }
    }

    fun getFavorites(context: Context, restID: Long) : LiveData<FavoritizedRestaurantsTableModel>? {
        liveDataFavorite = DatabaseRepository.getFavoritesDetails(context, restID)
        return liveDataFavorite
    }

<<<<<<< HEAD:RestaurantMe/app/src/main/java/com/codingwithmitch/navigationcomponentsexample/model/LoginViewModel.kt
    fun getAllFavorites(context: Context) : LiveData<List<FavoritizedRestaurantsTableModel>> {
        return LoginRepository.getAllFavorites(context)
=======
    fun getAllFavorites(context: Context) : LiveData<List<FavoritizedRestaurantsTableModel>>? {
        return DatabaseRepository.getAllFavorites(context)
>>>>>>> feature/FavoriteList:RestaurantMe/app/src/main/java/com/codingwithmitch/restaurantME/model/ViewModel.kt
    }

    fun removeFavorites(context: Context, restID: Long) {
        viewModelScope.launch(Dispatchers.IO) {
            DatabaseRepository.removeFavoritesDetails(context, restID)
        }
    }

    fun removeUnfavoritized(context: Context) {
        viewModelScope.launch(Dispatchers.IO) {
            delay(60000)
            DatabaseRepository.removeUnfavoritized(context)
        }
    }
}