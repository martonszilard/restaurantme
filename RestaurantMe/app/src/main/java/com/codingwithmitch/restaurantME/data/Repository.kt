package com.codingwithmitch.restaurantME.data

import android.util.Log
import com.codingwithmitch.restaurantME.api.ApiService
import com.codingwithmitch.restaurantME.model.RepoSearchResult
import com.codingwithmitch.restaurantME.model.Restaurant
import com.codingwithmitch.restaurantME.model.Supplier
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import retrofit2.HttpException
import java.io.IOException

private const val REPO_STARTING_PAGE_INDEX = 1

/**
 * Repository class that works with local and remote data sources.
 */

class Repository(private val service: ApiService) {

    // keep the list of all results received
    val inMemoryCache = mutableListOf<Restaurant>()

    // keep channel of results. The channel allows us to broadcast updates so
    // the subscriber will have the latest data
    private val searchResults = ConflatedBroadcastChannel<RepoSearchResult>()

    // keep the last requested page. When the request is successful, increment the page number.
    private var lastRequestedPage = REPO_STARTING_PAGE_INDEX

    // avoid triggering multiple requests in the same time
    private var isRequestInProgress = false

    /**
     * Search repositories whose names match the query, exposed as a stream of data that will emit
     * every time we get more data from the network.
     */
    @FlowPreview
    suspend fun getSearchResultStream(query: String): Flow<RepoSearchResult> {
        Log.d("Repository", "New query: $query")
        lastRequestedPage = 1
        inMemoryCache.clear()
        requestAndSaveData(query)

        return searchResults.asFlow()
    }

    suspend fun requestMore(query: String) {
        if(lastRequestedPage == 1) {
            ++lastRequestedPage
        }
        if (isRequestInProgress) return
        val successful = requestAndSaveData(query)
        if (successful) {
            lastRequestedPage++
        }
    }

    suspend fun retry(query: String) {
        if (isRequestInProgress) return
        requestAndSaveData(query)
    }

    private suspend fun requestAndSaveData(query: String): Boolean {
        isRequestInProgress = true
        var successful = false
        try {
            val response = service.searchRepos(query, lastRequestedPage, NETWORK_PAGE_SIZE)
            val restaurants = response.items ?: emptyList()
            inMemoryCache.addAll(restaurants)
            Supplier.restaurants.addAll(restaurants)
            Log.i("supplier fill", Supplier.restaurants.toString())
            //val restaurantsNames = reposByName(query)
            searchResults.offer(RepoSearchResult.Success(inMemoryCache))
            successful = true
        } catch (exception: IOException) {
            searchResults.offer(RepoSearchResult.Error(exception))
        } catch (exception: HttpException) {
            searchResults.offer(RepoSearchResult.Error(exception))
        }
        isRequestInProgress = false
        return successful
    }

    companion object {
        private const val NETWORK_PAGE_SIZE = 25
    }
}